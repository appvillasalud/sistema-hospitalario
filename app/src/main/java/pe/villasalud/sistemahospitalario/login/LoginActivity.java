package pe.villasalud.sistemahospitalario.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

//Volley
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pe.villasalud.sistemahospitalario.R;
import pe.villasalud.sistemahospitalario.common.Parametros;
import pe.villasalud.sistemahospitalario.descuento.DescuentoActivity;
import pe.villasalud.sistemahospitalario.version.AppUpdateChecker;

public class LoginActivity extends AppCompatActivity {
    private Button btn_ingresar;
    private EditText et_username, et_password;
    private RequestQueue requestQueue;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ocultarBarraEstado();
        //checkUpdate();
        setContentView(R.layout.activity_login);

        //Declaracion de Componentes
        btn_ingresar = findViewById(R.id.btn_ingresar);
        et_username = findViewById(R.id.et_username);
        et_password = findViewById(R.id.et_password);
        progressBar = findViewById(R.id.progressbar);

        //Componentes
        btn_ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarBarraProgreso();
                //btn_ingresar.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.btn_azul_sin_borde));
                //btn_ingresar.setTextColor(Color.parseColor("#FFFFFF"));
                btn_ingresar.setEnabled(false);
                inicioSession();
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        ocultarBarraEstado();
    }

    private  void inicioSession(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Parametros.URLCE + "login", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    JSONObject jsonjObject = new JSONObject(obj.toString());
                    if(jsonjObject.getString("ok").equals("true")){

                        //Recibiendo data
                        String data = jsonjObject.getString("data");
                        //JSONObject objData = new JSONObject(data);
                        //String iduser = objData.getString("iduser");
                        //String accessToken = objData.getString("accessToken");
                        //Log.e("LoginActivity:","iduser: "+iduser+" accessToken: "+accessToken);

                        //Pasando la respuesta a DescuentoActivity
                        SharedPreferences userdata = getSharedPreferences("userdata", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = userdata.edit();
                        editor.putString("userdata",data);
                        editor.apply();

                        finish();
                        Intent intent = new Intent(LoginActivity.this, DescuentoActivity.class);
                        startActivity(intent);
                    } else {
                        btn_ingresar.setEnabled(true);
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mostrarBarraProgreso();
                btn_ingresar.setEnabled(true);
                Toast.makeText(getApplicationContext(),"Usuario y/o Contraseña Incorrecta",Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> parametros = new HashMap<>();
                parametros.put("username",et_username.getText().toString());
                parametros.put("password",et_password.getText().toString());
                return parametros;
            }
        };
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void ocultarBarraEstado() {
        //getWindow().setStatusBarColor(Color.TRANSPARENT);
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        //| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    private void mostrarBarraProgreso(){
        if(progressBar.getVisibility() == View.GONE){
            progressBar.setVisibility(View.VISIBLE);
        }else{
            progressBar.setVisibility(View.GONE);
        }
    }
}