package pe.villasalud.sistemahospitalario.common;

public class VariableGlobal {
    private static int posSpEmpresa;

    public static int getPosSpEmpresa() {
        return posSpEmpresa;
    }

    public static void setPosSpEmpresa(int posSpEmpresa) {
        VariableGlobal.posSpEmpresa = posSpEmpresa;
    }
}
