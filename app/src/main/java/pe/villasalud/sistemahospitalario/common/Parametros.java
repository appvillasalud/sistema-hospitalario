package pe.villasalud.sistemahospitalario.common;

public class Parametros {

    /**
     * enlaces al entorno de desarrollo
     */
    //public static String URL_BASE = "http://168.121.223.121"; //ip externa
    /*public static String URL_BASE = "http://192.168.1.191:4000"; //ip local
    public static String URLCE = URL_BASE + "/api/";*/

    /**
      enlaces al entorno de producción
     */
    public static String URL_BASE = "https://atencionmedica.villasalud.pe/api-descuento";
    public static String URLCE = URL_BASE + "/";
}
