package pe.villasalud.sistemahospitalario.common;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Paint.Style;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import androidx.core.view.MotionEventCompat;

public class PlotPastelito extends View implements OnTouchListener {
    private int ancho;
    private int alto;
    private int AnguloTouch = 0;
    private float ZonaTouchX;
    private float TrasladaY;
    private int ColorTitulo = -16777216;
    private int ColorAco = -16777216;
    private int ColorGraficoText = -1;
    private int ColorFondo = Color.argb(255, 255, 255, 255);
    private int ColorContorno = -1;
    private int sizeT = 26;
    private int sizeAco = 24;
    private int sizeGraficoText = 20;
    private float PorcionCentral = 0.4F;
    private String STitulo = "Grafico de pastel";
    private String SError = "Error";
    private double[] porcentajes;
    String[] Etiquetas;
    private Paint slicePaint = new Paint();
    private Paint AcoText;
    private Paint GraficoText;
    private Paint Titulo;
    private Paint PaintContorno;
    private boolean Berror = false;
    private boolean BMuestraPorcentaje = true;
    private boolean ActivaTouch = true;
    private int color1;
    private int dedo1x;
    private int dedo1y;
    private int xinicial;
    private int yinicial;
    private int[] ArrayColores = new int[]{Color.rgb(255, 255, 255), Color.rgb(187, 23, 38), Color.rgb(255, 255, 102), Color.rgb(178, 255, 102), Color.rgb(102, 255, 102), Color.rgb(102, 204, 178), Color.rgb(102, 255, 255), Color.rgb(102, 178, 255), Color.rgb(102, 102, 255), Color.rgb(178, 102, 255), Color.rgb(255, 102, 255), Color.rgb(255, 102, 178), Color.rgb(94, 0, 0), Color.rgb(255, 255, 128), Color.rgb(255, 128, 64), Color.rgb(255, 187, 119), Color.rgb(128, 64, 0), Color.rgb(128, 128, 0), Color.rgb(170, 255, 170), Color.rgb(128, 215, 0), Color.rgb(0, 128, 0), Color.rgb(0, 64, 0), Color.rgb(0, 128, 128), Color.rgb(0, 128, 64), Color.rgb(0, 64, 64), Color.rgb(128, 255, 255), Color.rgb(0, 64, 128), Color.rgb(0, 0, 128), Color.rgb(64, 128, 128), Color.rgb(0, 128, 255), Color.rgb(0, 128, 192), Color.rgb(128, 128, 255), Color.rgb(0, 0, 160), Color.rgb(0, 0, 94), Color.rgb(255, 128, 192), Color.rgb(128, 128, 192), Color.rgb(128, 0, 64), Color.rgb(128, 0, 128), Color.rgb(64, 0, 64), Color.rgb(255, 128, 255), Color.rgb(255, 0, 255), Color.rgb(255, 0, 128), Color.rgb(128, 0, 255), Color.rgb(64, 0, 128)};
    private RectF rectf;
    private float[] datapoints;

    public PlotPastelito(Context lienzo, String titulo) {
        super(lienzo);
        this.STitulo = titulo;
        this.AcoText = new Paint();
        this.GraficoText = new Paint();
        this.Titulo = new Paint();
        this.PaintContorno = new Paint();
        this.TrasladaY = (float)this.sizeAco;
        this.SetHD(true);
        this.setFocusable(true);
        this.setOnTouchListener(this);
    }

    protected void onDraw(Canvas canvas) {
        this.alto = this.getHeight();
        this.ZonaTouchX = (float)this.ancho * 0.66F;
        this.ancho = this.getWidth();
        this.Setup();
        canvas.drawColor(this.ColorFondo);
        double RadioPastel;
        if ((double)(this.alto - this.sizeT) <= (double)this.ancho * 0.66D) {
            RadioPastel = (double)(this.alto - this.sizeT);
        } else {
            RadioPastel = (double)this.ancho * 0.66D;
        }

        canvas.drawText(this.STitulo, (float)(this.ancho / 3), (float)(this.sizeT - 3), this.Titulo);
        if (!this.Berror) {
            int startLeft = 0;
            int startTop = this.sizeT;
            int endBottom = (int)RadioPastel + this.sizeT;
            int endRight = (int)RadioPastel;
            int cx = (int)RadioPastel / 2;
            int cy = (int)((RadioPastel + (double)(2 * this.sizeT)) / 2.0D);
            this.rectf = new RectF((float)startLeft, (float)startTop, (float)endRight, (float)endBottom);
            float[] scaledValues = this.scale();
            float sliceStartPoint = (float)this.AnguloTouch;

            int i;
            for(i = 0; i < scaledValues.length; ++i) {
                if (i > 43) {
                    int r = (int)(Math.random() * 255.0D) + 1;
                    int g = (int)(Math.random() * 255.0D) + 1;
                    int b = (int)(Math.random() * 255.0D) + 1;
                    this.slicePaint.setColor(Color.rgb(r, g, b));
                } else {
                    this.slicePaint.setColor(this.ArrayColores[i]);
                }

                canvas.drawArc(this.rectf, sliceStartPoint, scaledValues[i], true, this.slicePaint);
                canvas.drawArc(this.rectf, sliceStartPoint, scaledValues[i], true, this.PaintContorno);
                if ((float)(this.sizeT + this.sizeAco * (i + 1) - this.sizeAco) + this.TrasladaY > (float)this.sizeT) {
                    /*canvas.drawRect((float)(0.66D * (double)this.ancho), (float)(this.sizeT + this.sizeAco * (i + 1) - this.sizeAco) + this.TrasladaY, (float)(0.66D * (double)this.ancho) + (float)this.sizeAco, (float)(this.sizeT + this.sizeAco * (i + 1)) + this.TrasladaY, this.slicePaint);
                    if (this.BMuestraPorcentaje) {
                        canvas.drawText("[" + this.datapoints[i] + "] " + this.Etiquetas[i], (float)(0.66D * (double)this.ancho) + (float)this.sizeAco + 3.0F, (float)(this.sizeT + this.sizeAco * (i + 1)) + this.TrasladaY, this.AcoText);
                    } else {
                        canvas.drawText("[" + this.porcentajes[i] + "%] " + this.Etiquetas[i], (float)(0.66D * (double)this.ancho) + (float)this.sizeAco + 3.0F, (float)(this.sizeT + this.sizeAco * (i + 1)) + this.TrasladaY, this.AcoText);
                    }*/
                }

                sliceStartPoint += scaledValues[i];
            }

            for(i = 0; i < scaledValues.length; ++i) {
                canvas.save();
                canvas.rotate(sliceStartPoint + scaledValues[i] / 2.0F, (float)((double)cx + RadioPastel * 0.3D * Math.cos((double)(sliceStartPoint + scaledValues[i] / 2.0F) * 3.14D / 180.0D)), (float)((double)cy + RadioPastel * 0.3D * Math.sin((double)(sliceStartPoint + scaledValues[i] / 2.0F) * 3.14D / 180.0D)));
                if (this.BMuestraPorcentaje) {
                    canvas.drawText("" + this.porcentajes[i] + "%", (float)((double)cx + RadioPastel * 0.3D * Math.cos((double)(sliceStartPoint + scaledValues[i] / 2.0F) * 3.14D / 180.0D)), (float)((double)cy + RadioPastel * 0.3D * Math.sin((double)(sliceStartPoint + scaledValues[i] / 2.0F) * 3.14D / 180.0D)), this.GraficoText);
                } else {
                    canvas.drawText("" + this.datapoints[i], (float)((double)cx + RadioPastel * 0.3D * Math.cos((double)(sliceStartPoint + scaledValues[i] / 2.0F) * 3.14D / 180.0D)), (float)((double)cy + RadioPastel * 0.3D * Math.sin((double)(sliceStartPoint + scaledValues[i] / 2.0F) * 3.14D / 180.0D)), this.GraficoText);
                }

                canvas.restore();
                sliceStartPoint += scaledValues[i];
            }

            if (this.PorcionCentral != 0.0F) {
                this.slicePaint.setColor(Color.argb(100, 255, 255, 255));
                canvas.drawCircle((float)cx, (float)cy, (float)((double)this.PorcionCentral * (RadioPastel / 2.0D) + 15.0D), this.slicePaint);
                this.slicePaint.setColor(this.ColorFondo);
                canvas.drawCircle((float)cx, (float)cy, (float)((double)this.PorcionCentral * (RadioPastel / 2.0D)), this.slicePaint);
            }
        } else {
            canvas.drawText(this.SError, 50.0F, 50.0F, this.Titulo);
        }

    }

    private void Setup() {
        this.slicePaint.setDither(true);
        this.slicePaint.setStyle(Style.FILL);
        this.PaintContorno.setDither(true);
        this.PaintContorno.setStyle(Style.STROKE);
        this.PaintContorno.setColor(this.ColorContorno);
        this.PaintContorno.setStrokeWidth(2.0F);
        this.AcoText.setTextSize((float)this.sizeAco);
        this.AcoText.setColor(this.ColorAco);
        this.AcoText.setFakeBoldText(true);
        this.GraficoText.setTextSize((float)this.sizeGraficoText);
        this.GraficoText.setColor(this.ColorGraficoText);
        this.GraficoText.setFakeBoldText(true);
        this.Titulo.setTextSize((float)this.sizeT);
        this.Titulo.setColor(this.ColorTitulo);
        this.Titulo.setFakeBoldText(true);
    }

    public boolean onTouch(View view, MotionEvent event) {
        if (this.ActivaTouch) {
            int action = MotionEventCompat.getActionMasked(event);
            int fingersCount = event.getPointerCount();
            switch(action) {
                case 0:
                    this.xinicial = (int)event.getX(0);
                    this.yinicial = (int)event.getY(0);
                    return true;
                case 1:
                    return true;
                case 2:
                    this.dedo1x = (int)event.getX(0);
                    this.dedo1y = (int)event.getY(0);
                    if (fingersCount == 1) {
                        if ((float)this.xinicial > this.ZonaTouchX) {
                            if (this.dedo1y > this.yinicial) {
                                this.TrasladaY += (float)this.sizeAco * 0.5F;
                            }

                            if (this.dedo1y < this.yinicial) {
                                this.TrasladaY -= (float)this.sizeAco * 0.5F;
                            }
                        } else {
                            if (this.dedo1y > this.yinicial) {
                                this.AnguloTouch += 4;
                            }

                            if (this.dedo1y < this.yinicial) {
                                this.AnguloTouch -= 4;
                            }
                        }
                    }

                    this.invalidate();
                    return true;
                default:
                    return super.onTouchEvent(event);
            }
        } else {
            return false;
        }
    }

    public void SetDatos(float[] datapoints, String[] etiquetas) {
        this.datapoints = datapoints;
        this.Etiquetas = etiquetas;
        if (datapoints.length == this.Etiquetas.length && datapoints.length != 0) {
            this.Berror = false;
        } else {
            this.SError = "Error la cantidad de etiquetas no es igual a la cantidad de datos";
            this.Berror = true;
        }

        this.ArrayPorcentajes(datapoints);
        this.invalidate();
    }

    private float[] scale() {
        float[] scaledValues = new float[this.datapoints.length];
        float total = this.getTotal();

        for(int i = 0; i < this.datapoints.length; ++i) {
            scaledValues[i] = this.datapoints[i] / total * 360.0F;
        }

        return scaledValues;
    }

    private float getTotal() {
        float total = 0.0F;
        float[] var2 = this.datapoints;
        int var3 = var2.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            float val = var2[var4];
            total += val;
        }

        return total;
    }

    private double[] ArrayPorcentajes(float[] datos) {
        this.porcentajes = new double[datos.length];
        double total = 0.0D;

        int i;
        for(i = 0; i < datos.length; ++i) {
            total += (double)datos[i];
        }

        for(i = 0; i < datos.length; ++i) {
            if (total != 0.0D) {
                this.porcentajes[i] = this.Redondear((double)datos[i] / total * 100.0D);
            }
        }

        return this.porcentajes;
    }

    private double Redondear(double numero) {
        double n = Double.parseDouble(String.format("%.3f", numero));
        return n;
    }

    public void SetTitulo(String t) {
        this.STitulo = t;
    }

    public void SetColorTitulo(int r, int g, int b) {
        this.ColorTitulo = Color.rgb(r, g, b);
    }

    public void SetColorAcot(int r, int g, int b) {
        this.ColorAco = Color.rgb(r, g, b);
    }

    public void SetColorTextGrafico(int r, int g, int b) {
        this.ColorGraficoText = Color.rgb(r, g, b);
    }

    public void SetColorFondo(int r, int g, int b) {
        this.ColorFondo = Color.rgb(r, g, b);
    }

    public void SetColorContorno(int r, int g, int b) {
        this.ColorContorno = Color.rgb(r, g, b);
    }

    public void SetColorDato(int dato, int r, int g, int b) {
        if (dato >= 1 && dato <= 44) {
            this.ArrayColores[dato - 1] = Color.rgb(r, g, b);
        }

    }

    public void SetSizeTitulo(int s) {
        this.sizeT = s;
    }

    public void SetSizeAcot(int s) {
        this.sizeAco = s;
    }

    public void SetSizeTextGrafico(int s) {
        this.sizeGraficoText = s;
    }

    public void SetSizeTitulo(String t) {
        this.STitulo = t;
    }

    public void SetShowPorcentajes(boolean p) {
        this.BMuestraPorcentaje = p;
    }

    public void SetHD(boolean hd) {
        if (hd) {
            this.slicePaint.setAntiAlias(true);
            this.AcoText.setAntiAlias(true);
            this.GraficoText.setAntiAlias(true);
            this.Titulo.setAntiAlias(true);
            this.PaintContorno.setAntiAlias(true);
        } else {
            this.slicePaint.setAntiAlias(false);
            this.AcoText.setAntiAlias(false);
            this.GraficoText.setAntiAlias(false);
            this.Titulo.setAntiAlias(false);
        }

        this.PaintContorno.setAntiAlias(false);
    }

    public void SetCentro(double c) {
        if (c <= 1.0D && c >= 0.0D) {
            this.PorcionCentral = (float)c;
        }

    }

    public void SetTouch(boolean t) {
        this.ActivaTouch = t;
    }
}