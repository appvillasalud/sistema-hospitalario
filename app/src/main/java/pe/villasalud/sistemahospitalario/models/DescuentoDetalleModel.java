package pe.villasalud.sistemahospitalario.models;

public class DescuentoDetalleModel {
    private String iddescuentotoken, producto, cantidad, v_unitario, porc_descuento;

    public String getIddescuentotoken() {
        return iddescuentotoken;
    }

    public void setIddescuentotoken(String iddescuentotoken) {
        this.iddescuentotoken = iddescuentotoken;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getV_unitario() {
        return v_unitario;
    }

    public void setV_unitario(String v_unitario) {
        this.v_unitario = v_unitario;
    }

    public String getPorc_descuento() {
        return porc_descuento;
    }

    public void setPorc_descuento(String porc_descuento) {
        this.porc_descuento = porc_descuento;
    }
}
