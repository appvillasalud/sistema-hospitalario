package pe.villasalud.sistemahospitalario.models;

public class EmpresaModel {
    private String idsedeempresaadmin, razonsocial, sede, razonsocialSede;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdsedeempresaadmin() {
        return idsedeempresaadmin;
    }

    public void setIdsedeempresaadmin(String idsedeempresaadmin) {
        this.idsedeempresaadmin = idsedeempresaadmin;
    }

    public String getRazonsocial() {
        return razonsocial;
    }

    public void setRazonsocial(String razonsocial) {
        this.razonsocial = razonsocial;
    }

    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }

    public String getRazonsocialSede() { return razonsocialSede; }

    public void setRazonsocialSede(String razonsocialSede) { this.razonsocialSede = razonsocialSede; }
}
