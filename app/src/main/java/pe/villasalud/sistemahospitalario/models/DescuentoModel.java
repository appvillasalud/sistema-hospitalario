package pe.villasalud.sistemahospitalario.models;

public class DescuentoModel {
    private String idsedeempresaadmin, iddescuentotoken, descripcion_caja, cajero, nro_documento, descuento, total, fecha, estado, idestado;
    private String cliente, tipoDocumentoCliente, numDocumento, edad, sexo;
    private String tipoDocumentoVenta, orden, idmediopago, subtotal, igv, recargo;

    public String getIdsedeempresaadmin() {
        return idsedeempresaadmin;
    }

    public void setIdsedeempresaadmin(String idsedeempresaadmin) {
        this.idsedeempresaadmin = idsedeempresaadmin;
    }

    public String getIddescuentotoken() {
        return iddescuentotoken;
    }

    public void setIddescuentotoken(String iddescuentotoken) {
        this.iddescuentotoken = iddescuentotoken;
    }

    public String getDescripcion_caja() {
        return descripcion_caja;
    }

    public void setDescripcion_caja(String descripcion_caja) {
        this.descripcion_caja = descripcion_caja;
    }

    public String getCajero() {
        return cajero;
    }

    public void setCajero(String cajero) {
        this.cajero = cajero;
    }

    public String getNro_documento() {
        return nro_documento;
    }

    public void setNro_documento(String nro_documento) {
        this.nro_documento = nro_documento;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getTipoDocumentoCliente() {
        return tipoDocumentoCliente;
    }

    public void setTipoDocumentoCliente(String tipoDocumentoCliente) {
        this.tipoDocumentoCliente = tipoDocumentoCliente;
    }

    public String getNumDocumento() {
        return numDocumento;
    }

    public void setNumDocumento(String numDocumento) {
        this.numDocumento = numDocumento;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getTipoDocumentoVenta() {
        return tipoDocumentoVenta;
    }

    public void setTipoDocumentoVenta(String tipoDocumentoVenta) {
        this.tipoDocumentoVenta = tipoDocumentoVenta;
    }

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }

    public String getIdmediopago() {
        return idmediopago;
    }

    public void setIdmediopago(String idmediopago) {
        this.idmediopago = idmediopago;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getIgv() {
        return igv;
    }

    public void setIgv(String igv) {
        this.igv = igv;
    }

    public String getRecargo() {
        return recargo;
    }

    public void setRecargo(String recargo) {
        this.recargo = recargo;
    }

    public String getIdestado() {
        return idestado;
    }

    public void setIdestado(String idestado) {
        this.idestado = idestado;
    }
}
