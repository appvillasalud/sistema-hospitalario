package pe.villasalud.sistemahospitalario.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import pe.villasalud.sistemahospitalario.R;
import pe.villasalud.sistemahospitalario.login.LoginActivity;

public class SplashActivity extends AppCompatActivity {
    ImageView imgFondoSplash;
    TextView textViewTituloSplash;
    boolean animacion, tokenizacion;

    Animation frombottom, fromtop;
    private static int splashTimeOut=3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ocultarBarraEstado();

        setContentView(R.layout.activity_splash);

        splash();
    }

    private void ocultarBarraEstado() {
        //getWindow().setStatusBarColor(Color.TRANSPARENT);
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ocultarBarraEstado();
        splash();
    }

    private void splash() {
        animacion = false;
        tokenizacion = false;

        imgFondoSplash = findViewById(R.id.imgFondoSplash);
        textViewTituloSplash = findViewById(R.id.textViewTituloSplash);

        frombottom = AnimationUtils.loadAnimation(this,R.anim.frombottom);
        fromtop = AnimationUtils.loadAnimation(this,R.anim.fromtop);

        imgFondoSplash.startAnimation(fromtop);
        textViewTituloSplash.startAnimation(frombottom);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                animacion = true;
                siguientePantalla();
            }
        },splashTimeOut);
    }

    private void siguientePantalla() {
        finish();
        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(intent);
    }
}