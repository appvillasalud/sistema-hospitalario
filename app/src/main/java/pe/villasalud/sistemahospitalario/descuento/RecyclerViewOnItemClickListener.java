package pe.villasalud.sistemahospitalario.descuento;

import android.view.View;

public interface RecyclerViewOnItemClickListener {
    void onClick(View v, int position);
}
