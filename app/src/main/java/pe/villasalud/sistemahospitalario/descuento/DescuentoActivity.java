package pe.villasalud.sistemahospitalario.descuento;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import pe.villasalud.sistemahospitalario.R;
import pe.villasalud.sistemahospitalario.common.Parametros;
import pe.villasalud.sistemahospitalario.common.VariableGlobal;
import pe.villasalud.sistemahospitalario.login.LoginActivity;
import pe.villasalud.sistemahospitalario.models.DescuentoModel;
import pe.villasalud.sistemahospitalario.models.EmpresaModel;

public class DescuentoActivity extends AppCompatActivity {

    //Variables
    private Spinner spEmpresa, spEstado;
    private ArrayList<String> empresaDatos = new ArrayList<>(), estadoDatos = new ArrayList<>();
    ArrayList<Descuento> data = new ArrayList<>();
    private TextView tv_detalleSede;
    private RequestQueue requestQueue;

    private ArrayList<EmpresaModel> empresaModelArrayList;
    private ArrayList<DescuentoModel> descuentoModelArrayList;

    private int idEstado = 3;
    private int posSpEmpresa = 0;
    private String idEmpresa = "0";
    //private int idsedeempresaadmin = 9;
    private String accessToken;

    //RecyclerView rvDescuento
    private RecyclerView rvDescuento;
    private GridLayoutManager glm;
    private AdapterDescuento adapterDescuento;

    //Popup
    private Dialog myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ocultarBarraEstado();
        setContentView(R.layout.activity_descuento);

        posSpEmpresa = VariableGlobal.getPosSpEmpresa();
        Log.e("getPos", "posSpEmpresa"+posSpEmpresa);

        try {
            //Carga iduser y accesstoken desde LoginActivity
            setUserData();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Componentes
        rvDescuento = findViewById(R.id.rv_descuento);
        spEmpresa = findViewById(R.id.sp_empresa);
        spEstado = findViewById(R.id.sp_estado);
        tv_detalleSede = findViewById(R.id.tv_detalleSede);

        //Cargar Spinner Data
        getSpinnerEmpresaData();
        getSpinnerEstadoData();

        //Spinner spEmpresa
        spEmpresa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                tv_detalleSede.setText(empresaModelArrayList.get(pos).getSede());
                idEmpresa = empresaModelArrayList.get(pos).getIdsedeempresaadmin();
                VariableGlobal.setPosSpEmpresa(pos);
                getData();
                Log.e("DescuentoActivity","idEmpresa: "+idEmpresa+" posSpEmpresa: "+posSpEmpresa);
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        //Spinner spEstado
        spEstado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                switch (parent.getItemAtPosition(pos).toString()){
                    case "POR APROBAR":
                        idEstado = 3;
                        getData();
                        break;
                    case "APROBADO":
                        idEstado = 1;
                        getData();
                        break;
                    case "RECHAZADO":
                        idEstado = 2;
                        getData();
                        break;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        getData();
    }

    private void getSpinnerEstadoData() {
        estadoDatos.add("POR APROBAR");
        estadoDatos.add("APROBADO");
        estadoDatos.add("RECHAZADO");
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(this, R.layout.spinner_item, estadoDatos);
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
        spEstado.setGravity(Gravity.CENTER);
        spEstado.setAdapter(spinnerArrayAdapter);
    }

    private void setUserData() throws JSONException {
        SharedPreferences userdata = this.getSharedPreferences("userdata", Context.MODE_PRIVATE);
        String sessiondata = userdata.getString("userdata","userdata");
        JSONObject objuserdata  = new  JSONObject(sessiondata);
        JSONObject jsonUserdata = new JSONObject(objuserdata.toString());
        //String iduser = jsonUserdata.getString("iduser");
        accessToken = jsonUserdata.getString("accessToken");
        Log.e("accessToken", accessToken);
    }

    private void getSpinnerEmpresaData(){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Parametros.URLCE + "empresa-admin", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.optString("ok").equals("true")) {
                        empresaModelArrayList = new ArrayList<>();
                        JSONArray dataArray = obj.getJSONArray("rows");
                        for (int i = 0; i < dataArray.length(); i++) {
                            EmpresaModel empresaModel = new EmpresaModel();
                            JSONObject dataobj = dataArray.getJSONObject(i);
                            empresaModel.setId(i);
                            empresaModel.setIdsedeempresaadmin(dataobj.getString("idsedeempresaadmin"));
                            empresaModel.setRazonsocial(dataobj.getString("razon_social"));
                            empresaModel.setSede(dataobj.getString("sede"));
                            String sedeAbr;
                            switch (dataobj.getString("sede")){
                                case "VILLA EL SALVADOR":
                                    sedeAbr = "VES";
                                    break;
                                case "SAN JUAN DE LURIGANCHO":
                                    sedeAbr = "SJL";
                                    break;
                                default:
                                    sedeAbr = dataobj.getString("sede");
                                    break;
                            }
                            String razonSede = dataobj.getString("razon_social")+" - "+sedeAbr;
                            Log.e("razonSede", "razonSede "+razonSede);
                            empresaModel.setRazonsocialSede(dataobj.getString("razon_social")+" - "+sedeAbr);

                            empresaModelArrayList.add(empresaModel);
                        }
                        for (int i = 0; i < empresaModelArrayList.size(); i++) {
                            empresaDatos.add(empresaModelArrayList.get(i).getRazonsocialSede());
                        }
                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_item, empresaDatos);
                        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
                        spEmpresa.setAdapter(spinnerArrayAdapter);

                        spEmpresa.setSelection(posSpEmpresa);
                        Log.e("pos"," "+posSpEmpresa);

                        tv_detalleSede.setText(empresaModelArrayList.get(0).getSede());
                    }else{
                        Toast.makeText(getApplicationContext(),"Error al cargar la lista de empresas", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Error al cargar la lista de empresas",Toast.LENGTH_SHORT).show();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+accessToken);
                return  headers;
            }
        };
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void getData() {
        data.clear();
        Log.e("WS","token: "+accessToken+" url:"+Parametros.URLCE + "descuento/"+idEmpresa+"/"+idEstado);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Parametros.URLCE + "descuento/"+idEmpresa+"/"+idEstado, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.e("DRM response",response);
                    JSONObject obj = new JSONObject(response);
                    if (obj.optString("ok").equals("true")) {
                        descuentoModelArrayList = new ArrayList<>();
                        JSONArray dataArray = obj.getJSONArray("rows");
                            for (int i = 0; i < dataArray.length(); i++) {

                                DescuentoModel descuentoModel = new DescuentoModel();
                                JSONObject dataobj = dataArray.getJSONObject(i);

                                descuentoModel.setIddescuentotoken(dataobj.getString("iddescuentotoken"));
                                descuentoModel.setIdsedeempresaadmin(dataobj.getString("idsedeempresaadmin"));
                                descuentoModel.setDescripcion_caja(dataobj.getString("descripcion_caja"));
                                descuentoModel.setCajero(dataobj.getString("cajero"));
                                descuentoModel.setNro_documento(dataobj.getString("nro_documento"));

                                String total = dataobj.getString("total");
                                if (total.contains(".")) {
                                    switch (total.length() - total.indexOf(".") - 1) {
                                        case 1:
                                            descuentoModel.setTotal(total + "0");
                                            break;
                                        default:
                                            descuentoModel.setTotal(total);
                                            break;
                                    }
                                } else {
                                    descuentoModel.setTotal(total + ".00");
                                }

                                String descuento = dataobj.getString("descuento");
                                if (descuento.contains(".")) {
                                    switch (descuento.length() - descuento.indexOf(".") - 1) {
                                        case 1:
                                            descuentoModel.setDescuento(descuento + "0");
                                            break;
                                        default:
                                            descuentoModel.setDescuento(descuento);
                                            break;
                                    }
                                } else {
                                    descuentoModel.setDescuento(descuento + ".00");
                                }

                                String estado = dataobj.getString("estado");
                                descuentoModel.setIdestado(estado);
                                switch (estado) {
                                    case "1":
                                        descuentoModel.setEstado("Aprobado");
                                        break;
                                    case "2":
                                        descuentoModel.setEstado("Rechazado");
                                        break;
                                    case "3":
                                        descuentoModel.setEstado("Por Aprobar");
                                        break;
                                }

                                String fecha = dataobj.getString("fecha");
                                if (!fecha.equals("null")) {
                                    String fechaF = dataobj.getString("fecha").substring(0, 10);
                                    descuentoModel.setFecha(fechaF);
                                } else {
                                    descuentoModel.setFecha("0000/00/00");
                                }
                                descuentoModelArrayList.add(descuentoModel);
                            }

                        for (int j = 0; j < descuentoModelArrayList.size(); j++) {
                            //int i = (descuentoModelArrayList.size()-1)-j; //Ordenamos del mas actual al mas antiguo
                            data.add(
                                    new Descuento(
                                            descuentoModelArrayList.get(j).getIddescuentotoken(),
                                            descuentoModelArrayList.get(j).getFecha(),
                                            descuentoModelArrayList.get(j).getNro_documento(),
                                            descuentoModelArrayList.get(j).getTotal(),
                                            descuentoModelArrayList.get(j).getDescuento(),
                                            descuentoModelArrayList.get(j).getDescripcion_caja(),
                                            descuentoModelArrayList.get(j).getCajero(),
                                            descuentoModelArrayList.get(j).getEstado(),
                                            descuentoModelArrayList.get(j).getIdestado(),
                                            descuentoModelArrayList.get(j).getIdsedeempresaadmin()
                                    )
                            );
                        }

                        glm = new GridLayoutManager(getApplicationContext(), 1);
                        rvDescuento.setLayoutManager(glm);
                        adapterDescuento = new AdapterDescuento(data);
                        rvDescuento.setAdapter(adapterDescuento);
                    }else{
                        Toast.makeText(getApplicationContext(),"Error al cargar la lista de empresas", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Error al cargar la lista de empresas",Toast.LENGTH_SHORT).show();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+accessToken);
                return  headers;
            }
        };
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

        //return data;
    }

    @Override
    protected void onResume() {
        super.onResume();
        ocultarBarraEstado();
    }

    private void ocultarBarraEstado() {
        //getWindow().setStatusBarColor(Color.TRANSPARENT);
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                //View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        //| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                         View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        //| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        //| View.SYSTEM_UI_FLAG_FULLSCREEN
                        //| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        );

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    /*protected void onPause(){
        super.onPause();
        SharedPreferences userdata = getSharedPreferences("userdata", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = userdata.edit();
        editor.remove("userdata");
        editor.clear();
        editor.apply();
        finishAffinity();
    }*/

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
            myDialog = new Dialog(this);
            myDialog.setContentView(R.layout.dialog_cerrar_sesion);
            myDialog.setCancelable(false);
            TextView tvTitulo, tvMessage,  btn_cancelar, btn_aceptar;
            tvTitulo = myDialog.findViewById(R.id.tvTitulo);
            tvMessage = myDialog.findViewById(R.id.tvMessage);
            btn_aceptar = myDialog.findViewById(R.id.btn_aceptar);
            btn_cancelar = myDialog.findViewById(R.id.btn_cancelar);
            btn_aceptar.setText("SALIR");
            btn_cancelar.setText("CANCELAR");

            /*tvTitulo.setText("Atención!");
            tvMessage.setText("¿Esta seguro que desea salir?");*/

        btn_aceptar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    myDialog.dismiss();
                    SharedPreferences userdata = getSharedPreferences("userdata", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = userdata.edit();
                    editor.remove("userdata");
                    editor.clear();
                    editor.apply();
                    finish();
                    Intent intent = new Intent(DescuentoActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            });
        btn_cancelar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    myDialog.dismiss();
                }
            });
            myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            myDialog.show();
        }
}