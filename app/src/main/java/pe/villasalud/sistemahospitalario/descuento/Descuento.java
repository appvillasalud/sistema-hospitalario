package pe.villasalud.sistemahospitalario.descuento;

public class Descuento {
    private String id, fecha, documento, descuento, total, caja, cajero, estado, idestado, idsedeempresaadmin;

    public Descuento() {
    }

    public Descuento(String id, String fecha, String documento, String total, String descuento, String caja, String cajero, String estado, String idestado, String idsedeempresaadmin) {
        this.id = id;
        this.fecha = fecha;
        this.documento = documento;
        this.descuento = descuento;
        this.total = total;
        this.caja = caja;
        this.cajero = cajero;
        this.estado = estado;
        this.idsedeempresaadmin = idsedeempresaadmin;
        this.idestado = idestado;
    }

    public String getId() {
        return id;
    }

    public void setId(String fecha) {
        this.id = fecha;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCaja() {
        return caja;
    }

    public void setCaja(String caja) {
        this.caja = caja;
    }

    public String getCajero() {
        return cajero;
    }

    public void setCajero(String cajero) {
        this.cajero = cajero;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getIdestado() {
        return idestado;
    }

    public void setIdestado(String idestado) {
        this.idestado = idestado;
    }

    public String getIdsedeempresaadmin() {
        return idsedeempresaadmin;
    }

    public void setIdsedeempresaadmin(String idsedeempresaadmin) {
        this.idsedeempresaadmin = idsedeempresaadmin;
    }
}
