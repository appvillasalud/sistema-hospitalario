package pe.villasalud.sistemahospitalario.descuento.detalleDescuento;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import pe.villasalud.sistemahospitalario.R;
import pe.villasalud.sistemahospitalario.common.Parametros;
import pe.villasalud.sistemahospitalario.descuento.AdapterDescuento;
import pe.villasalud.sistemahospitalario.descuento.Descuento;
import pe.villasalud.sistemahospitalario.descuento.DescuentoActivity;
import pe.villasalud.sistemahospitalario.login.LoginActivity;
import pe.villasalud.sistemahospitalario.models.DescuentoDetalleModel;
import pe.villasalud.sistemahospitalario.models.DescuentoModel;

public class DetalleDescuentoActivity extends AppCompatActivity {

    //Componentes
    private Button btn_cerrar, btn_aprobar, btn_desaprobar;
    private TextView tv_nombre, tv_tipoDoc, tv_numDoc, tv_edad, tv_sexo, tv_orden, tv_tipoPago, txt_boleta, tv_numBoleta, tv_caja, tv_cajero, tv_subTotal, tv_descuento, tv_igv, tv_total;

    //RecyclerView rvDescuento
    private RecyclerView rvDetalleDescuento;
    private GridLayoutManager glm;
    private AdapterDetalleDescuento adapterDetalleDescuento;
    private ArrayList<DetalleDescuento> data = new ArrayList<>();
    private ArrayList<DescuentoDetalleModel> descuentoDetalleModelArrayList;
    private String iddescuentotoken, idsedeempresaadmin, idestado;
    private String accessToken;
    private RequestQueue requestQueue;

    //Popup
    private Dialog myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ocultarBarraEstado();
        setContentView(R.layout.activity_detalle_descuento);
        iddescuentotoken = getIntent().getStringExtra("iddescuentotoken");
        idsedeempresaadmin = getIntent().getStringExtra("idsedeempresaadmin");
        idestado = getIntent().getStringExtra("idestado");


        Log.e(" DetalleDesc ","iddescuentotoken: "+iddescuentotoken+" idsedeempresaadmin: "+idsedeempresaadmin+" idestado: "+idestado);

        //Componentes
        rvDetalleDescuento = findViewById(R.id.rv_detalleDescuento);
        btn_cerrar = findViewById(R.id.btn_cerrar);
        btn_aprobar = findViewById(R.id.btn_aprobar);
        btn_desaprobar = findViewById(R.id.btn_desaprobar);


        tv_nombre = findViewById((R.id.tv_nombre));
        tv_tipoDoc = findViewById((R.id.tv_tipoDoc));
        tv_numDoc = findViewById((R.id.tv_numDoc));
        tv_edad = findViewById((R.id.tv_edad));
        tv_sexo = findViewById((R.id.tv_sexo));
        tv_orden = findViewById((R.id.tv_orden));
        tv_tipoPago = findViewById((R.id.tv_tipoPago));
        txt_boleta = findViewById((R.id.txt_boleta));
        tv_numBoleta = findViewById((R.id.tv_numBoleta));
        tv_caja = findViewById((R.id.tv_caja));
        tv_cajero = findViewById((R.id.tv_cajero));
        tv_subTotal = findViewById((R.id.tv_subTotal));
        tv_descuento = findViewById((R.id.tv_descuento));
        tv_igv = findViewById((R.id.tv_igv));
        tv_total = findViewById((R.id.tv_total));

        try {
            //Carga iduser y accesstoken desde LoginActivity
            setUserData();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        btn_cerrar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(DetalleDescuentoActivity.this, DescuentoActivity.class);
                startActivity(intent);
            }
        } );

        if(idestado.equals("3"))
        {
            btn_desaprobar.setVisibility(View.VISIBLE);
            btn_aprobar.setVisibility(View.VISIBLE);
        }
        else
        {
            btn_desaprobar.setVisibility(View.GONE);
            btn_aprobar.setVisibility(View.GONE);
        }

        btn_desaprobar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertaPopUp(2);
            }
        } );

        btn_aprobar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertaPopUp(1);
            }
        } );

        getData();
    }

    private void getData() {
        data.clear();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Parametros.URLCE + "descuento/"+idsedeempresaadmin+"/"+idestado, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.optString("ok").equals("true")) {
                        descuentoDetalleModelArrayList = new ArrayList<>();
                        JSONArray dataArray = obj.getJSONArray("rows");
                        for (int i = 0; i < dataArray.length(); i++) {
                            DescuentoDetalleModel descuentoDetalleModel = new DescuentoDetalleModel();
                            JSONObject dataobj = dataArray.getJSONObject(i);

                            if (dataobj.getString("data_json").equals("null")) {
                                Log.e("DescuentoActivity", dataobj.getString("iddescuentotoken") + " data_json es nulo");
                            } else {
                                if(iddescuentotoken.equals(dataobj.getString("iddescuentotoken"))) {
                                //Obtenemos informacion del array anidado data_json
                                if (dataobj.has("data_json")) {

                                    JSONObject datajsonObj = dataobj.getJSONObject("data_json");
                                    JSONObject datosObj = datajsonObj.getJSONObject("datos");
                                    JSONArray detalleArray = datajsonObj.getJSONArray("detalle");

                                    Log.e(" DetalleDescXX ",""+dataobj.getString("iddescuentotoken")+" y iddescuentotoken:"+iddescuentotoken);

                                    tv_nombre.setText(datosObj.getString("cliente"));
                                    tv_tipoDoc.setText(datosObj.getString("tipoDocumentoCliente"));
                                    tv_numDoc.setText(datosObj.getString("numDocumento"));
                                    tv_edad.setText(datosObj.getString("edad"));
                                    tv_sexo.setText(datosObj.getString("sexo"));
                                    tv_orden.setText(datosObj.getString("orden"));

                                    switch (datosObj.getString("idmediopago")){
                                        case "1":
                                            tv_tipoPago.setText("AL CONTADO");
                                            break;
                                        case "2":
                                            tv_tipoPago.setText("VISA");
                                            break;
                                        case "5":
                                            tv_tipoPago.setText("MASTERCARD");
                                            break;
                                        case "6":
                                            tv_tipoPago.setText("MIXTO");
                                            break;
                                        case "7":
                                            tv_tipoPago.setText("AMERICAN EXPRESS");
                                            break;
                                        case "8":
                                            tv_tipoPago.setText("DINNERS CLUB");
                                            break;
                                        default:
                                            tv_tipoPago.setText("-");
                                            break;
                                    }

                                    txt_boleta.setText(datosObj.getString("tipoDocumentoVenta"));

                                    tv_numBoleta.setText(dataobj.getString("nro_documento"));
                                    tv_caja.setText(dataobj.getString("descripcion_caja"));
                                    tv_cajero.setText(dataobj.getString("cajero"));
                                    tv_subTotal.setText(datosObj.getString("subtotal"));

                                    String descuento = dataobj.getString("descuento");
                                    if (descuento.contains(".")) {
                                        switch (descuento.length() - descuento.indexOf(".") - 1) {
                                            case 1:
                                                tv_descuento.setText(dataobj.getString("descuento")+"0");
                                                break;
                                            default:
                                                tv_descuento.setText(dataobj.getString("descuento"));
                                                break;
                                        }
                                    } else {
                                        tv_descuento.setText(dataobj.getString("descuento")+".00");
                                    }

                                    tv_igv.setText(datosObj.getString("igv"));
                                    tv_total.setText(datosObj.getString("total"));

                                    for (int j = 0; j < detalleArray.length(); j++) {
                                        descuentoDetalleModel.setProducto(detalleArray.getJSONObject(j).getString("producto"));
                                        descuentoDetalleModel.setCantidad(detalleArray.getJSONObject(j).getString("cantidad"));
                                        descuentoDetalleModel.setV_unitario(detalleArray.getJSONObject(j).getString("v_unitario"));

                                        //float desc = 100-Float.parseFloat(detalleArray.getJSONObject(j).getString("porc_descuento"));
                                        descuentoDetalleModel.setPorc_descuento(detalleArray.getJSONObject(j).getString("porc_descuento"));

                                        descuentoDetalleModelArrayList.add(descuentoDetalleModel);
                                        data.add(new DetalleDescuento(descuentoDetalleModelArrayList.get(j).getProducto(), descuentoDetalleModelArrayList.get(j).getCantidad(), descuentoDetalleModelArrayList.get(j).getPorc_descuento(), descuentoDetalleModelArrayList.get(j).getV_unitario()));
                                    }
                                }
                                }
                            }

                            glm = new GridLayoutManager(getApplicationContext(), 1);
                            rvDetalleDescuento.setLayoutManager(glm);
                            adapterDetalleDescuento = new AdapterDetalleDescuento(data);
                            rvDetalleDescuento.setAdapter(adapterDetalleDescuento);
                        }
                    }else{
                        Toast.makeText(getApplicationContext(),"Error al cargar la lista de empresas", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Error al cargar la lista de empresas",Toast.LENGTH_SHORT).show();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+accessToken);
                return  headers;
            }
        };
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    private void setUserData() throws JSONException {
        SharedPreferences userdata = this.getSharedPreferences("userdata", Context.MODE_PRIVATE);
        String sessiondata = userdata.getString("userdata","userdata");
        JSONObject objuserdata  = new  JSONObject(sessiondata);
        JSONObject jsonUserdata = new JSONObject(objuserdata.toString());
        //String iduser = jsonUserdata.getString("iduser");
        accessToken = jsonUserdata.getString("accessToken");
    }

    @Override
    protected void onResume() {
        super.onResume();
        ocultarBarraEstado();
    }

    private void alertaPopUp(int siono){
        myDialog = new Dialog(this);
        myDialog.setContentView(R.layout.dialog_cerrar_sesion);
        myDialog.setCancelable(false);
        TextView tvTitulo, tvMessage, btn_cancelar, btn_aceptar;
        tvTitulo = myDialog.findViewById(R.id.tvTitulo);
        tvMessage = myDialog.findViewById(R.id.tvMessage);
        btn_aceptar = myDialog.findViewById(R.id.btn_aceptar);
        btn_cancelar = myDialog.findViewById(R.id.btn_cancelar);
        btn_cancelar.setText("CANCELAR");

        switch (siono){
            case 1:
                btn_aceptar.setText("APROBAR");
                tvTitulo.setText("Atención!");
                tvMessage.setText("¿Estás seguro que desea APROBAR la solicitud?");

                btn_aceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        myDialog.dismiss();
                        Toast.makeText(getApplicationContext(),"SOLICITUD APROBADA",Toast.LENGTH_SHORT).show();
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, Parametros.URLCE + "descuento/aprobar", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),"Error al realizar la accion",Toast.LENGTH_SHORT).show();
                    }
                }){
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String,String> headers = new HashMap<>();
                        headers.put("Authorization", "Bearer "+accessToken);
                        return  headers;
                    }
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> parametros = new HashMap<>();
                        parametros.put("id",iddescuentotoken);
                        return parametros;
                    }
                };
                requestQueue = Volley.newRequestQueue(getApplicationContext());
                requestQueue.add(stringRequest);
                        Intent intent = new Intent(DetalleDescuentoActivity.this, DescuentoActivity.class);
                        startActivity(intent);
                    }
                });
                break;
            case 2:
                btn_aceptar.setText("RECHAZAR");
                tvTitulo.setText("Atención!");
                tvMessage.setText("¿Estás seguro que desea DESAPROBAR la solicitud?");

                btn_aceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        myDialog.dismiss();
                        Toast.makeText(getApplicationContext(),"SOLICITUD RECHAZADA",Toast.LENGTH_SHORT).show();
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, Parametros.URLCE + "descuento/desaprobar", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),"Error al realizar la accion",Toast.LENGTH_SHORT).show();
                    }
                }){
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String,String> headers = new HashMap<>();
                        headers.put("Authorization", "Bearer "+accessToken);
                        return  headers;
                    }
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> parametros = new HashMap<>();
                        parametros.put("id",iddescuentotoken);
                        return parametros;
                    }
                };
                requestQueue = Volley.newRequestQueue(getApplicationContext());
                requestQueue.add(stringRequest);
                        Intent intent = new Intent(DetalleDescuentoActivity.this, DescuentoActivity.class);
                        startActivity(intent);
                    }
                });
                break;
        }

        btn_cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myDialog.dismiss();
            }
        });
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
        Intent intent = new Intent(DetalleDescuentoActivity.this, DescuentoActivity.class);
        startActivity(intent);
    }

    /*protected void onPause(){
        super.onPause();
        SharedPreferences userdata = getSharedPreferences("userdata", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = userdata.edit();
        editor.remove("userdata");
        editor.clear();
        editor.apply();
        finishAffinity();
    }*/

    private void ocultarBarraEstado() {
        //getWindow().setStatusBarColor(Color.TRANSPARENT);
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                //View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        //| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                         View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        //| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        //| View.SYSTEM_UI_FLAG_FULLSCREEN
                        //| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        );

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}