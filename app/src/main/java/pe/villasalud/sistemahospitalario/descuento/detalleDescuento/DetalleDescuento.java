package pe.villasalud.sistemahospitalario.descuento.detalleDescuento;

public class  DetalleDescuento {
    private String producto, cantidad, descuento, valorUnitario;

    public DetalleDescuento() {
    }

    public DetalleDescuento(String producto, String cantidad, String descuento, String valorUnitario) {
        this.producto = producto;
        this.cantidad = cantidad;
        this.descuento = descuento;
        this.valorUnitario = valorUnitario;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public String getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(String valorUnitario) {
        this.valorUnitario = valorUnitario;
    }
}
