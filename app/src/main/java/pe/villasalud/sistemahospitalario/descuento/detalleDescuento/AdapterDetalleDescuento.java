package pe.villasalud.sistemahospitalario.descuento.detalleDescuento;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import pe.villasalud.sistemahospitalario.R;
import pe.villasalud.sistemahospitalario.common.PlotPastelito;

public class AdapterDetalleDescuento extends RecyclerView.Adapter<AdapterDetalleDescuento.DescuentoViewHolder>{
    private ArrayList<DetalleDescuento> data;

    //Grafico PIE
    LinearLayout ll_pie;
    PlotPastelito pie;
    Context context;

    public AdapterDetalleDescuento(ArrayList<DetalleDescuento> data) {
        this.data = data;
    }

    @Override
    public DescuentoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DescuentoViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cv_detalle_descuento, parent, false));
    }

    @Override
    public void onBindViewHolder(DescuentoViewHolder holder, int position) {
        final DetalleDescuento descuento = data.get(position);
        holder.tv_producto.setText(descuento.getProducto());
        holder.tv_cantidad.setText(descuento.getCantidad());
        //holder.tv_descuento.setText(descuento.getDescuento()+"%");
        holder.tv_valorUnitario.setText(descuento.getValorUnitario());

        /*final Float desc1 = Float.valueOf(descuento.getDescuento());
        final Float desc2 = 100-desc1;

        float[] datapoints = {desc1,desc2};
        String[] etiquetas={"",""};
        pie.SetDatos(datapoints,etiquetas);
        pie.SetCentro(0);
        pie.SetSizeTextGrafico(0);
        pie.SetShowPorcentajes(false);
        pie.SetColorContorno(187,23,38);
        //pie.SetHD(true);
        if(pie.getParent() != null) {
            ((ViewGroup)pie.getParent()).removeView(pie); // <- fix
        }
        ll_pie.addView(pie);*/

        //Toast.makeText(v.getContext(), "Datos: "+descuento.getCantidad(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class DescuentoViewHolder extends RecyclerView.ViewHolder{

        //TextView tv_producto, tv_cantidad, tv_descuento, tv_valorUnitario;
        TextView tv_producto, tv_cantidad, tv_valorUnitario;

        public DescuentoViewHolder(View itemView) {
            super(itemView);
            tv_producto = itemView.findViewById(R.id.tv_producto);
            tv_cantidad = itemView.findViewById(R.id.tv_cantidad);
            //tv_descuento = itemView.findViewById(R.id.tv_descuento);
            tv_valorUnitario = itemView.findViewById(R.id.tv_valorUnitario);
            //ll_pie = itemView.findViewById(R.id.ll_pie);
            //pie = new PlotPastelito(itemView.getContext(),"");

        }
    }
}
