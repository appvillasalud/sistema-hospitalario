package pe.villasalud.sistemahospitalario.descuento;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import pe.villasalud.sistemahospitalario.R;
import pe.villasalud.sistemahospitalario.descuento.detalleDescuento.DetalleDescuentoActivity;

public class AdapterDescuento extends RecyclerView.Adapter<AdapterDescuento.DescuentoViewHolder>{
    private ArrayList<Descuento> data;
    private View.OnClickListener listener;

    public AdapterDescuento(ArrayList<Descuento> data) {
        this.data = data;
    }

    @Override
    public DescuentoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DescuentoViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cv_descuento, parent, false));
    }

    @Override
    public void onBindViewHolder(DescuentoViewHolder holder, int position) {
        final Descuento descuento = data.get(position);
        //holder.btn_detalle.setText(descuento.getId());
        holder.tv_fecha.setText(descuento.getFecha());
        holder.tv_documento.setText(descuento.getDocumento());
        holder.tv_descuento.setText(descuento.getDescuento());
        holder.tv_total.setText(descuento.getTotal());
        holder.tv_caja.setText(descuento.getCaja());
        holder.tv_cajero.setText(descuento.getCajero());
        holder.tv_estado.setText(descuento.getEstado());

        holder.idEmpresa = descuento.getIdsedeempresaadmin();
        holder.idEstado = descuento.getIdestado();
        Log.e(" DetalleDesc "," idsedeempresaadmin: "+descuento.getIdsedeempresaadmin()+" idestado: "+descuento.getIdestado());

        holder.btn_detalle.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("AdapterDescuento: ","Click "+descuento.getId());

                //Toast.makeText(v.getContext(), "Datos: "+descuento.getId(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(v.getContext(), DetalleDescuentoActivity.class);
                // Pasaremos de la actividad actual a OtraActivity
                intent.putExtra("iddescuentotoken", descuento.getId());
                intent.putExtra("idsedeempresaadmin", descuento.getIdsedeempresaadmin());
                intent.putExtra("idestado", descuento.getIdestado());
                v.getContext().startActivity(intent);
            }
        } );

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class DescuentoViewHolder extends RecyclerView.ViewHolder{

        TextView tv_fecha, tv_documento, tv_descuento, tv_total, tv_caja, tv_cajero, tv_estado;
        Button btn_detalle;
        String idEmpresa, idEstado;

        public DescuentoViewHolder(View itemView) {
            super(itemView);
            btn_detalle = itemView.findViewById(R.id.btn_detalle);
            tv_fecha = itemView.findViewById(R.id.tv_fecha);
            tv_documento = itemView.findViewById(R.id.tv_documento);
            tv_descuento = itemView.findViewById(R.id.tv_descuento);
            tv_total = itemView.findViewById(R.id.tv_total);
            tv_caja = itemView.findViewById(R.id.tv_caja);
            tv_cajero = itemView.findViewById(R.id.tv_cajero);
            tv_estado = itemView.findViewById(R.id.tv_estado);
        }
    }
}
